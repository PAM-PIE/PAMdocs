.. _particle_detection:

##############################
Particle Detection for Phasor
##############################


This module is designed to detect particles or other regions (e.g. cells) and pool all pixels for the individual regions for better signal-to-noise.
As this module is currently in a very early development stage, its shape and functions are changing rapidly.
Therefore, it will be explained only very briefly at this moment.

The program requires framewise phasor data from **PAM** (*.phf* files) that are loaded and displayed.

At this moment, a threshold and a wavelet based detection algorithms are implemented.

This algorithms use the intensity information of the file itself or alternatively an external mask (e.g. a second color channel), based on TIFF data, to detect the regions.

The data is then saved as a single, summed up image  with an averaged phasor for all pixels of an individual region.
The phasors of the pixels not assigned can be either saved normally or set to zero.

Alternatively, the data can be saved as a trace for each particle. Hereby, an image is created, where the x-axis corresponds to the frame and the y-axis to the particle number.

Both types of data are saves as *.phr* files to be analysed the the phasor module.